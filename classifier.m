function varargout = classifier(varargin)
% CLASSIFIER M-file for classifier.fig
%      CLASSIFIER, by itself, creates a new CLASSIFIER or raises the existing
%      singleton*.
%
%      H = CLASSIFIER returns the handle to a new CLASSIFIER or the handle to
%      the existing singleton*.
%
%      CLASSIFIER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CLASSIFIER.M with the given input arguments.
%
%      CLASSIFIER('Property','Value',...) creates a new CLASSIFIER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before classifier_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to classifier_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help classifier

% Last Modified by GUIDE v2.5 22-Mar-2012 16:11:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @classifier_OpeningFcn, ...
                   'gui_OutputFcn',  @classifier_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before classifier is made visible.
function classifier_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to classifier (see VARARGIN)

% Choose default command line output for classifier
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes classifier wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = classifier_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_File.
function Load_File_Callback(hObject, eventdata, handles)
% hObject    handle to Load_File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename path] = uigetfile('*.mat');
if filename == 0
    return;
end
s = [path filename];
temp  = load(s);
a = fieldnames(temp);
img = getfield(temp,a{1});
handles.img = img;
h = imagesc(abs(img));
% Add Pixel Information tool, specifying image as parent
% hpixinfo = impixelinfo(h);
% % Add Display Range tool, specifying image as parent
% hdrange = imdisplayrange(h);
axis equal;
axis tight;
set(handles.filename,'string',s);
handles.path = path;
handles.f = filename;
guidata(hObject, handles);
% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over Load_File.
function Load_File_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to Load_File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function class_size_Callback(hObject, eventdata, handles)
% hObject    handle to class_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of class_size as text
%        str2double(get(hObject,'String')) returns contents of class_size as a double
handles.c = str2double(get(hObject,'String'));
%msgbox(num2str(handles.class_size));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function class_size_CreateFcn(hObject, eventdata, handles)
% hObject    handle to class_size (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.c = 9;
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String', num2str(handles.c));
guidata(hObject, handles);


% --- Executes on button press in start.
function start_Callback(hObject, eventdata, handles)
% hObject    handle to start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
n = 100;
[x y] = ginput(n);
handles.x = x;
handles.y = y;
guidata(hObject, handles);


% --- Executes on button press in class_piixels.
function class_piixels_Callback(hObject, eventdata, handles)
% hObject    handle to class_piixels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% a = uitable;
% v = handles.x;
% w = handles.y;
X = round([handles.y, handles.x]); 
% set(a,'Data',X);
tablesize = size(X);    % This demo data is 24-by-3

% Define parameters for a uitable (col headers are fictional)
colnames = {'X', 'Y'};
% All column contain numeric data (integers, actually)
colfmt = {'numeric', 'numeric'};
% Disallow editing values (but this can be changed)
coledit = [false false];
% Set columns all the same width (must be in pixels)
colwdt = {60 60};
htable = uitable('Units', 'normalized',...
                 'Position', [.8 .15 .20 .55],...
                 'Data',  X,... 
                 'ColumnName', colnames,...
                 'ColumnFormat', colfmt,...
                 'ColumnWidth', colwdt,...
                 'ColumnEditable', coledit,...
                 'ToolTipString',...
                 'Select cells to highlight them on the plot',...
                 'CellSelectionCallback',{@select_callback,handles});
handles.htable = htable;
guidata(hObject, handles);

% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
filename = ['class_pixels_' handles.f];
pixels = round([handles.y handles.x]);
%b = get(handles.class_size,'String');
box_size = handles.c;
% save filename box_size pixels;
cd(handles.path);
eval(sprintf('save %s box_size pixels',filename));
s = fullfile(handles.path,filename);
set(handles.save_filename,'string',s);

function select_callback(hObject, eventdata, handles)
    % hObject    Handle to uitable1 (see GCBO)
    % eventdata  Currently selected table indices
    % Callback to erase and replot markers, showing only those
    % corresponding to user-selected cells in table. 
    % Repeatedly called while user drags across cells of the uitable

        axes(handles.axes1);
        h = imagesc(abs(handles.img));
        hold on;
        % Get the list of currently selected table cells
        sel = eventdata.Indices;     % Get selection indices (row, col)
                                     % Noncontiguous selections are ok
%         selcols = unique(sel(:,2));  % Get all selected data col IDs
        table = get(hObject,'Data'); % Get copy of uitable data
        x = table(sel(1,1),1);
        y = table(sel(1,1),2);
        scatter(y,x,100,'r','filled');
        
        
        
        
