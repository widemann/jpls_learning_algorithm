function Y = find_coordinates(U)
% find the the representation for each ROI in terms of the principal
% components. 
box_size = sqrt(size(U,1));
direc = uigetdir();
A = dir(direc);
Y = [];
j = 1;
for k = 3:length(A)
    s = A(k).name;
    if strncmpi(s,'labeled_classes_',16)
        temp = load(s);
        fn = temp.filename;
        fn = fn(17:end);
        img = load([fn '.mat']);
        img = img.(fn);
        [r c] = size(img);
        pixels = temp.dat;
        z = 1;
        if mod(box_size,2) == 1
            z = 0;
        end
        for m = 1:size(pixels,1)
            t = floor(box_size/2);
            x = pixels{m,1}-t:pixels{m,1}+t-z;
            y = pixels{m,2}-t:pixels{m,2}+t-z;
            x(x<1) = []; x(x>r) = [];
            y(y<1) = []; y(y>c) = [];
            seg = img(x,y);
            seg = seg(:);
            u = mean(seg);
            sig = std(seg);
            if sig < eps
                sig = eps;
            end
            BB = (seg-u)./sig;
            % just to make sure the vector is the right size. 
            v = zeros(box_size^2,1); v(1:length(seg)) = BB;
            Y(:,j) = U\v;
            j = j + 1;
        end
    end
end






