function create_weka_file(Y, class)
% this function writes an *.arff file so that it can be loaded into WEKA. 
% it contains the representation and class information for the ROI pixels.
% Y is the representation in terms of the principal components. Just
% the components that are chosen. 
s = datestr(now);
for k = length(s):-1:1
    if (strcmp(s(k),' ') || strcmp(s(k),':') || strcmp(s(k),'-'))
        s(k) = '_';
    end
end
filename = [s '.arff'];
fid = fopen(filename,'wt');
fprintf(fid,'@RELATION walls\n\n');

for k = 1:size(Y,1)
    fprintf(fid,sprintf('@ATTRIBUTE pc%d\tREAL\n',k));
end

fprintf(fid,'@ATTRIBUTE class\t{y,n}\n');
fprintf(fid,'@data\n');

for k = 1:size(Y,2)
    for j = 1:size(Y,1)
        fprintf(fid,'%6.2f,',Y(j,k));
    end
    if class(k) == 1
        fprintf(fid, 'y\n');
    else
        fprintf(fid, 'n\n');
    end
end

fclose(fid);