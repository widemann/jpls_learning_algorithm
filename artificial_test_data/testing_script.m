%% This script gives an easy example of how to use the algorith for
% a classification problem.
%% set the paths.
p = pwd;
cd ..
pp = genpath(pwd);
addpath(pp);
cd(p);
clear p pp;
%% step 1. 
classifier
% load in the file 'training_img_noise.mat'
% Try to click in the "middle" pixel.
% When done save the results. The file class_pixels_training_img_noise.mat'
% will be created. 

% step 2.
h_filter = create_filter();
% make sure you are in the directory that has
% 'class_pixels_training_img_noise.mat'

% step 3
mmax = 20; % arbitrary, but should be greater than the number of targets. 
R = create_roi(h_filter, mmax);
% choose the file 'training_img_noise.mat'

% step 4, label Regions of Interst gui
label_roi
% load 'roi_training_img_noise.mat'
% label and save the file. There should be a file called:
% 'labeled_classes_training_img_noise.mat'

% step 5
box_size = size(h_filter,1);
[U S V class] = svd_labeled_classes(box_size);
% make sure you are in the directory with the files that start with 
% 'labeled_classes_..."

% step 6
U = U(:,1:20);  % ended up choosing the first 2 columns.
display_eigenvectors(U);
U = U(:,1:2);

% step 7
Y = find_coordinates(U);
% make sure you are in the directory with the files that start with 
% 'labeled_classes_..."

% skip step 8

% step 9 (Apply to non-training data)
mmax = 50; % arbitrary, but should be at least as big as the number of 
           % targets in the img. 
[A p] = compute_probabilities(U, Y, class, h_filter, mmax);
% load 'test_img_noise.mat' twice. I know it's silly, but I'm lazy. 
% If you look at the code you'll see why. 

% step 10 (see the results)
t = 1; 
display_results(A,p,t);
% choose the file 'test_img_noise.mat'. 












