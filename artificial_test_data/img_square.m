function img = img_square(varargin)
% this function creates an image with squares in them. (all the same size for
% right now) If a square hits a border then it becomes a rectangle. 

if nargin == 0
    r = 100;
    c = r;
    w = 5;
    N = 10; % number of squares.
    noise = 1;
end
%
img = zeros(r,c);
for m = 1:N  
    % just trying to keep them off the sides. 
    LLC = [w+ceil((r-4*w)*rand(1)),w+ceil((c-4*w)*rand(1))]; %LLC = lower left corner
    x = LLC(1):LLC(1)+w;
    y = LLC(2):LLC(2)+w;
    x(x<0) = []; x(x>r) = [];
    y(y<0) = []; y(y>c) = [];

    points = [repmat(x(1),numel(y),1) y';...
              repmat(x(end),numel(y),1) y';...
              x' repmat(y(1),numel(x),1);...
              x' repmat(y(end),numel(x),1)];

    %can this be done w/o the for loop?
    for k = 1:size(points,1)
        img(points(k,1),points(k,2)) = 1;
    end
end

img = img + noise*rand(r,c);

figure, imagesc(img)




