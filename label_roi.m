function varargout = label_roi(varargin)
% LABEL_ROI M-file for label_roi.fig
%      LABEL_ROI, by itself, creates a new LABEL_ROI or raises the existing
%      singleton*.
%
%      H = LABEL_ROI returns the handle to a new LABEL_ROI or the handle to
%      the existing singleton*.
%
%      LABEL_ROI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LABEL_ROI.M with the given input arguments.
%
%      LABEL_ROI('Property','Value',...) creates a new LABEL_ROI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before label_roi_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to label_roi_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help label_roi

% Last Modified by GUIDE v2.5 11-Feb-2010 16:59:40

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @label_roi_OpeningFcn, ...
                   'gui_OutputFcn',  @label_roi_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before label_roi is made visible.
function label_roi_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to label_roi (see VARARGIN)

% Choose default command line output for label_roi
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes label_roi wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = label_roi_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in load_roi.
function load_roi_Callback(hObject, eventdata, handles)
% hObject    handle to load_roi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename path] = uigetfile('*.mat');
if filename == 0
    return;
end
s = [path filename];
temp  = load(s);
a = fieldnames(temp);
data = getfield(temp,a{1});
s2 = fullfile(path,filename(5:end));
tmp = load(s2);
name = fieldnames(tmp);
img = tmp.(name{1});
axes(handles.axes1);
h = imagesc(abs(img));
handles.img = img;
hold on;
% Add Pixel Information tool, specifying image as parent
% hpixinfo = impixelinfo(h);
% % Add Display Range tool, specifying image as parent
% hdrange = imdisplayrange(h);
axis equal;
axis tight;
set(handles.filename,'string',s);
handles.path = path;
f = data{1,1};
f = f(1:end-4);
handles.f = f;
b = data{1,2};
[i j v] = find(b);
X = [i j]; 
L = length(i);
dat = cell(L,3);
for k = 1:length(i)
    dat{k,1} = i(k);
    dat{k,2} = j(k);
    dat{k,3} = false;
end
% set(a,'Data',X);
tablesize = size(dat);    

% Define parameters for a uitable (col headers are fictional)
colnames = {'X', 'Y', 'check if in class'};
% All column contain numeric data (integers, actually)
colfmt = {'numeric', 'numeric', []};
% Disallow editing values (but this can be changed)
coledit = [false false true];
% Set columns all the same width (must be in pixels)
colwdt = {60 60 90};
htable = uitable('Units', 'normalized',...
                 'Position', [.75 .15 .25 .7],...
                 'Data',  dat,... 
                 'ColumnName', colnames,...
                 'ColumnFormat', colfmt,...
                 'ColumnWidth', colwdt,...
                 'ColumnEditable', coledit,...
                 'ToolTipString',...
                 'Select cells to highlight them on the plot',...
                 'CellSelectionCallback',{@select_callback,handles});
handles.htable = htable;
guidata(hObject, handles);


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dat = get(handles.htable,'Data');
filename = ['labeled_classes_' handles.f];

% save filename box_size pixels;
cd(handles.path);
eval(sprintf('save %s filename dat',filename));
s = fullfile(handles.path,filename);
set(handles.save_filename,'string',s);

function select_callback(hObject, eventdata, handles)
    % hObject    Handle to uitable1 (see GCBO)
    % eventdata  Currently selected table indices
    % Callback to erase and replot markers, showing only those
    % corresponding to user-selected cells in table. 
    % Repeatedly called while user drags across cells of the uitable

        axes(handles.axes1);
        h = imagesc(abs(handles.img));
        hold on;
        % Get the list of currently selected table cells
        sel = eventdata.Indices;     % Get selection indices (row, col)
                                     % Noncontiguous selections are ok
%         selcols = unique(sel(:,2));  % Get all selected data col IDs
        table = get(hObject,'Data'); % Get copy of uitable data
        x = table{sel(1),1};
        y = table{sel(1),2};
        scatter(y,x,100,'r','filled');
