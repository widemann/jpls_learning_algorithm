function display_results(A,p,t)
L = (p(:,1)./p(:,2) > t);
%v = find(L==1);
[filename direc] = uigetfile('*.mat');
temp = load(fullfile(direc,filename));
names = fieldnames(temp);
img = getfield(temp,names{1});
figure, imagesc(abs(img));
hold on;
[r c] = size(img);
[i j] = ind2sub([r c],A(L));
scatter(j,i,100,'r','filled')
