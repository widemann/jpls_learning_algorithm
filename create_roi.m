function R = create_roi(h, mmax, varargin)
% applies the filter to an image and finds Regions of Interest (ROIs).
% if a pixel is above the threshold, t = varargin{1}, then that pixel is 
% considered a region of interest.
% R is a cell that contains the training file name and the pixels in the 
% file that are considered a ROIs. (these pixels are 1's)
% h is the filter.
% mmax is the maximum number of ROIs. 
% varargin{1} = filename, the name of the file to be read in. 
% varargin{2} is the threshold. 

if nargin > 2
    filename = varargin{1};
    if nargin > 3
        thresh = varargin{2};
%     else
%         v = cell(8,1);
%         v{1} = 'shasta_hh_1_18.mat';
%         v{2} = 'shasta_vv_1_18.mat';
%         v{3} = 'vinson3_hh_total.mat';
%         v{4} = 'vinson3_vv_total.mat';
%         v{5} = 'vinson5_hh_total.mat';
%         v{6} = 'vinson5_vv_total.mat';
%         v{7} = 'vinson_HH_1_59.mat';
%         v{8} = 'vinson_vv_1_40.mat';
    end
else
    filename = uigetfile('*.mat');
end

% find the training files. 
% if nargin == 1
%     A = dir;
%     for k = 1:length(A)
%         v{k} = A(k).name;
%     end
% end

j = 1;
b = size(h,1);
% even/odd case
z = 1;
if mod(b,2) == 1
    z = 0;
end
t = floor(b/2);
v{1} = filename;
for k = 1:length(v)
    file = v{k};
%     if (strcmpi(file(end-3:end),'.mat') &&...
%             ~strncmpi(file(1:12),'class_pixels',12) &&...
%             ~strncmpi(file(1:12),'class_pixels',12))
        % for each training file, apply the filter
        R{j,1} = file;
        struc = load(file);
        img = abs(struc.(file(1:end-4)));
        [r c] = size(img);
        temp = zeros(r,c);
        % important: The logic below is basically saying that no
        % ROIs occur within ceil(b/2) pixels of the boundary of the image.
        % This may not always be true.
        for m = t+1:r-(t-z)
            for n = t+1:c-(t-z)
                x = m-t:m+t-z;
                y = n-t:n+t-z;
                A = img(x,y);
                u = mean(A(:));
                sig = std(A(:));
                if sig < eps
                    sig = Inf; % this used to be eps. 
                end
                BB = (A-u)./sig;
                D = BB.*h;
                temp(m,n) = sum(D(:));
            end
        end
        %temp = imfilter(img,h,'same');
        if ~exist('thresh','var')
            % take the 30 biggest. (arbitrary)
            % that are some distance apart. 
            d = 2; % distance
            %mmax = 30; % max number of ROIs to keep.
            R{j,2} = reduce_rois(temp, d, mmax);
            j = j+1;
        else
            C = abs(temp) > thresh;
            % save the coordinates of pixels above t
            B = temp.*C;
            R{j,2} = B;
            j = j+1;
        end
    %end
end

roi{1,1} = R{1,1};
roi{1,2} = R{1,2};
s = R{1,1};
eval(sprintf('save roi_%s roi',s));
        
function reduced = reduce_rois(img, d, mmax)
% this function reduces the number of ROIs. 
% It makes sure that every two ROIs are at least a distance d, in 
% the 'el-1' metric,from each other.
% d is distance.
% mmax = max number of ROIs per image.  
    [a idx] = sort(img(:), 'descend');
    v = find(a==0);
    if ~isempty(v)
    	a = a(1:v(1)-1);
        idx = idx(1:v(1)-1);
    end
    % there is probably a much smarter way to do this. 
    flag = 1;
    m = 1;
    [r c] = size(img);
    while flag 
        [i j] = ind2sub([r c],idx(m));
        x = i-d:i+d;
        y = j-d:j+d;
        %x = boundary_check(x,r); 
        x(x<1) = []; x(x>r) = [];
        %y = boundary_check(y,c); 
        y(y<1) = []; y(y>c) = [];
        [X Y] = meshgrid(x,y);
        P = [X(:) Y(:)];
        v = find(P(:,1) == i & P(:,2) == j);
        P(v,:) = [];
        idx2 = sub2ind([r c],P(:,1),P(:,2));
        [C IA IB] = intersect(idx,idx2);
        IA = sort(IA,'descend');
        for q = 1:length(IA)
            a(IA(q)) = [];
            idx(IA(q)) = [];
        end
        m = m+1;
        if m > length(idx)
            flag = 0;
        end
        if ((mmax~=0) && (m > mmax))
            flag = 0;
        end
    end
    if mmax 
        if length(idx) > mmax
            idx(mmax+1:end) = [];
        end
    end
    tmp = zeros(r,c);
    tmp(idx) = 1;
    reduced = img.*tmp;   











