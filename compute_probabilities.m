function [A p] = compute_probabilities(U, Y, class, h_filter,mmax, varargin)
% computes the probability of being in target class versus non-target
% A contains the ROIs for the image. 
% p(i,j,1) contains the probability pixel (i,j) is in the target class.
% p(i,j,2) contains the probability pixel (i,j) is in the non-target class.
% img is image to be processed.
% img_filename is the filename of img. This is necessary for the create_roi
% fcn.
% U is a subset of the columns of U from the output of SVD_labeled_classes
% Y is coordinate representation, i.e. the output of find_coordinates.
% class is one of the outputs from SVD_labeled_classes
% h_filter is the filter
% mmax is the max number of ROIs to find. 
if nargin > 5
    img_filename = varargin{1};
else
    [img_filename direc] = uigetfile('*.mat');
    temp = load(fullfile(direc,img_filename));
    names = fieldnames(temp);
    img = getfield(temp,names{1});
end
    
box_size = size(h_filter,1);
z = 1;
if mod(box_size,2) == 1
    z = 0;
end
t = floor(box_size/2);
% 0. Find the the representation y for each ROI in the img. 
R = create_roi(h_filter,mmax);
[r c] = size(R{1,2});
A = find(R{1,2} ~= 0);
coords = zeros(size(U,2), numel(A));
for k = 1:size(A,1)
    [i j] = ind2sub([r c],A(k));
    x = i-t:i+t-z;
    y = j-t:j+t-z;
    x(x<1) = []; x(x>r) = [];
    y(y<1) = []; y(y>c) = [];
    seg = img(x,y);
    seg = seg(:);
    u = mean(seg);
    sig = std(seg);
    if sig < eps
        sig = eps;
    end
    BB = (seg-u)./sig;
    v = zeros(box_size^2,1);
    v(1:length(seg)) = BB;
    coords(:,k) = U\v;
end

% 1. compute the mean and covariance matrix for the target class.
v = find(class == 1);
mean_target = mean(Y(:,v),2);
cov_target = cov((Y(:,v))');

% 2. compute the mean and covariance matrix for the non-target class.
w = find(class == 0);
mean_NT = mean(Y(:,w),2);
cov_NT = cov((Y(:,w))');

% 3. compute p(y|T) = N(y; mean_target, cov_target)
% 4. compute p(y|NT) = N(y; mean_NT, cov_NT)
inv_cov_target = inv(cov_target);
inv_cov_NT = inv(cov_NT);
re = 1e-5;
pvalue = zeros(size(coords,2),2);
% if this errors out try using less principal components, i.e. less columns
% of U. 
for k = 1:size(coords,2)
    t = (coords(:,k) - mean_target);
    r1 = sqrt(t'*inv_cov_target*t);
    pvalue(k,1) = real(1 - mvnlps(mean_target, cov_target, mean_target, inv_cov_target, r1, re));
    t = (coords(:,k) - mean_NT);
    r1 = sqrt(t'*inv_cov_NT*t);
    pvalue(k,2) = real(1 - mvnlps(mean_NT, cov_NT, mean_NT, inv_cov_NT, r1, re));
end

% 5. compute p(T|y) = (p(y|T)p(T))/(p(y|T)p(T) + p(y|NT)p(NT)),  p(y|T) = N(y; mu, sigma)
% 6. compute p(NT|y) = (p(y|NT)p(NT))/(p(y|T)p(T) + p(y|NT)p(NT))
P_T = length(v)/length(class); 
P_NT = length(w)/length(class);
a = pvalue(:,1)*P_T; 
b = pvalue(:,2)*P_NT;
p = zeros(size(coords,2),2);
p(:,1) = a./(a+b);
p(:,2) = b./(a+b);

















