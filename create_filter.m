function h = create_filter(varargin)
% read in the classification files and create a filter.
% h is the filter
direc = uigetdir();%'C:\Documents and Settings\dwidemann\Desktop\Learning_Walls\data';
% maybe use
%direc = uigetdir();
files = dir(direc);
h = [];
for k = 3:length(files)
    s = files(k).name;
    if strncmpi(s,'class_pixels',12)
        load(fullfile(direc,files(k).name));
        % pixels and box_size should now be loaded. 
        % load the backprojected image. 
        img = s(14:end);
        struc = load(fullfile(direc,img));
        [r c] = size(struc.(img(1:end-4)));
        % even/odd case
        z = 1;
        if mod(box_size,2) == 1
            z = 0;
        end
        % initialize the filter. 
        if ~any(h(:))
            h = zeros(box_size);
        end
        if size(h,1) ~= box_size
            error('box size must be the same for all classification pixels.');
        end
        % add the image segments to h to create a filter. 
        for m = 1:size(pixels,1)
            t = floor(box_size/2);
            x = pixels(m,1)-t:pixels(m,1)+t-z;
            y = pixels(m,2)-t:pixels(m,2)+t-z;
            x(x<1) = []; x(x>r) = [];
            y(y<1) = []; y(y>c) = [];
            temp = struc.(img(1:end-4));
            % might take off the abs later. 
            A = zeros(size(h));
            B = abs(temp(x,y));
            u = 0; %u = mean(B(:));
            sig = 1; %sig = std(B(:));
            % not sure about putting "them" in the upper-left corner. 
            A(1:numel(x),1:numel(y)) = (B - u)./sig; 
            h = h + A;
        end
        clear struc;
    end
end

% normalize h
h = h/sum(abs(h(:)));
    













