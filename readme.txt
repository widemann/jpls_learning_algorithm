Learning to Recognize Volcanoes on Venus

This folder contains an implementation of the algorithm in the article
"Learning to Recognize Volcanoes on Venus" by Burl, et al.
Journal: Machine Learning
volume: 30
pages: 165-195
year: 1998

If you want to get started right away go to the artificial_test_data folder and run testing_script.m.

Brief summary of the algorithm:

A single class of objects in images, e.g. volcanoes, are to be detected and classified. 

step 1: The images from the training set are loaded into the GUI and objects within each
image are classified. 

step 2: The above chosen objects are used to create a convolution filter. This filter is 
applied to each image in the training set. 

step 3: Each filtered pixel that is above a certain threshold is classified as a 
region of interest (ROI).  

step 4: PCA is performed on the regions of interest. 

step 5: The k eigenvectors that best capture the class features are selected. This is 
usually eigenvectors 1 through k, corresponding to the k-biggest eigenvalues. 

step 6: A person goes through each ROI in it's k-dimensional representation and
classifies it as a target or not a target. 

step 7: The distributions for the target class and non-target class are created. 
Currently, they are assumed to be Gaussian, so we compute the k-dimensional mean, mu,
and the k x k dimensional covariance matrix, sigma. 

step 8: Calculating class probabilities. Let y be the k-dimensional representation
of a ROI and T represent the target class. NT represent the non-target class. 
Then the probability of being in the target class given y is: (Bayes Rule)

    p(T|y) = (p(y|T)p(T))/(p(y|T)p(T) + p(y|NT)p(NT)),

    p(y|T) = N(y; mu, sigma)


Files, Order of Use:

1. classifier GUI.
2. h = create_filter()
3. R = create_roi(h, mmax) 
4. label_roi GUI
5. [U S V classes] = svd_labeled_classes
6. display_eigenvectors (choose which ones to use, usually U = U(:,1:k))
7. Y = find_coordinates(U(:,1:k))
8. (optional) create_weka_file
9. [A p] = compute_probabilities(U,Y,classes,mmax)
10. display_results(A,p,t)

David Widemann 
SRI International
david.widemann@sri.com
February 2010

Thoughts: The rub occurs in steps 4 and 5. If a better representation can be found, 
a sparser representation, then I think the algorithm will perform better.

