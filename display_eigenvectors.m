function display_eigenvectors(U)
[r c] = size(U);
s = sqrt(r);
h = figure;
%maximize(h);
k = 1;
for i = 1:s
    for j = 1:s
        if k <= c
            subplot(s,s,(i-1)*s+j)
            A = reshape(U(:,(i-1)*s+j),s,s);
            imagesc(A);
            axis off
            title(sprintf('PC %d',(i-1)*s+j));
            k = k + 1;
        end
    end
end
% maximize(h);    


